package com.example.listfragement;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class CountryListFragement extends ListFragment {
	String[] countries = {
	        "India",
	        "Pakistan",
	        "Sri Lanka",
	        "China",
	        "Bangladesh",
	        "Nepal",
	        "Afghanistan",
	        "North Korea",
	        "South Korea",
	        "Japan"
	        
	    };
	ArrayAdapter<String> adapter;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	 adapter=new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, countries);
	
	setListAdapter(adapter);
 		return super.onCreateView(inflater, container, savedInstanceState);
	}
	// item click
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	Toast.makeText(getActivity(), "Clicked On "+ adapter.getItem(position), Toast.LENGTH_SHORT).show();
		super.onListItemClick(l, v, position, id);
	}
	// long click
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	
		super.onActivityCreated(savedInstanceState);
		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(getActivity(), "On Item Long Click "+ adapter.getItem(position), Toast.LENGTH_SHORT).show();
				return false;
			}
		});
	}

}
